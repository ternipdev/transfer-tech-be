var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001,
  multer = require('multer'),
  path = require('path'),
  logger = require('morgan'),
  mongoose = require('mongoose')
cors = require('cors')
mongoose.Promise = global.Promise
mongoose
  .connect('mongodb://localhost:27017/TransferTech', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => {
    console.log('Connected !!!')
  })
  .catch((err) => {
    console.log(err)
  })

app.use(
  cors({
    origin: '*', // allow to server to accept request from different origin
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true // allow session cookie from browser to pass through
  })
)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger('dev'))
app.use(express.static(path.join(__dirname, 'public/images')))
let diskStorage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, path.join(__dirname, 'public/images'))
  },
  filename: (req, file, callback) => {
    console.log(file.originalname)
    callback(null, file.originalname)
  }
})
app.use(multer({ storage: diskStorage }).any())
var routes = require('./API/Router')
routes(app)

app.use(function (req, res) {
  res.status(404).send({ url: req.originalUrl + ' not found' })
})

app.listen(port)

console.log('Server started on: ' + port)
