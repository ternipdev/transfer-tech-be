module.exports = function (app) {
  const DemoController = require('../Controller/index')
  const subscriptionHandler = require('../Controller/subscriptionHandler')
  app
    .route('/api/v1/oneone/book')
    .get(DemoController.getBook)
    .post(DemoController.postBook)
  app
    .route('/api/v1/oneone/owner')
    .get(DemoController.getOwner)
    .post(DemoController.postOwner)
  app.route('/api/v1/oneone/owner/:ownerId').put(DemoController.updateOwner)
  app.route('/api/v1/upload-multi').post(DemoController.uploadMultiFile)

  app.post(
    '/subscription',
    subscriptionHandler.handlePushNotificationSubscription
  )
  app.get('/subscription/:id', subscriptionHandler.sendPushNotification)
}
